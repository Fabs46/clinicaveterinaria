﻿namespace LibraryVeterinaryClinic
{
    /// <summary>
    /// Model Doctor
    /// </summary>
    public class Doctor
    {
        #region Atributes

        private int startShift;

        private int endShift;

        #endregion

        #region Properties

        public int IdDoctor { get; set; }
 
        public string NameDoctor { get; set; }

        public string SurnameDoctor { get; set; }

        public string Room { get; set; }

        public int StartShift
        {
            get
            {
                return startShift;
            }

            set
            {
                if (value >= 8)
                {
                    startShift = value;
                }

                else
                {
                    startShift = 8;
                }
            }
        }

        public int EndShift
        {
            get
            {
                return endShift;
            }

            set
            {
                if (value > startShift && value < 21)
                {
                    endShift = value;
                }

                else
                {
                    endShift = 19;
                }
            }
        }


        public string DoctorInfo
        {
            get
            {
                return $"{IdDoctor} - {NameDoctor} {SurnameDoctor} Room: {Room} Start Shift: {startShift} End Shift: {endShift}";
            }
        }


        #endregion

    }
}
