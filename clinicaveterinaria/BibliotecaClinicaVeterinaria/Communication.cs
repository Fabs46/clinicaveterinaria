﻿
namespace LibraryVeterinaryClinic
{
    using System.Collections.Generic;

    public class Communication
    {

        public int IdCom { get; set; }

        public Client Client { get; set; }

        public string Message { get; set; }


        public string CommunicationInfo
        {

            get
            {
                return $" Message nr {IdCom}" +
                    $" to {Client.NameClient} " +
                    $" phone nr {Client.Phone}" +
                    $" Email {Client.Email}" +
                    $" Message -> {Message} \n ";

            }
        }
    }
}
