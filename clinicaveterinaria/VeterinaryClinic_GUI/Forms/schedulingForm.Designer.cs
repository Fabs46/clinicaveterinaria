﻿namespace VeterinaryClinic_GUI.Forms
{
    partial class SchedulingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.doctorsComboBox = new System.Windows.Forms.ComboBox();
            this.clientsComboBox = new System.Windows.Forms.ComboBox();
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.scheduleButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.appointmentListBox = new System.Windows.Forms.ListBox();
            this.petsComboBox = new System.Windows.Forms.ComboBox();
            this.appointmentTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.hoursNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.hoursNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose Doctor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Choose Client";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Choose Client\'s Pet";
            // 
            // doctorsComboBox
            // 
            this.doctorsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.doctorsComboBox.FormattingEnabled = true;
            this.doctorsComboBox.Location = new System.Drawing.Point(181, 48);
            this.doctorsComboBox.Name = "doctorsComboBox";
            this.doctorsComboBox.Size = new System.Drawing.Size(362, 21);
            this.doctorsComboBox.TabIndex = 3;
            // 
            // clientsComboBox
            // 
            this.clientsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clientsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clientsComboBox.FormattingEnabled = true;
            this.clientsComboBox.Location = new System.Drawing.Point(181, 81);
            this.clientsComboBox.Name = "clientsComboBox";
            this.clientsComboBox.Size = new System.Drawing.Size(362, 21);
            this.clientsComboBox.TabIndex = 4;
            this.clientsComboBox.SelectedValueChanged += new System.EventHandler(this.clientsComboBox_SelectedValueChanged);
            // 
            // monthCalendar
            // 
            this.monthCalendar.Location = new System.Drawing.Point(654, 29);
            this.monthCalendar.MaxSelectionCount = 1;
            this.monthCalendar.MinDate = new System.DateTime(2019, 1, 3, 0, 0, 0, 0);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 6;
            this.monthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar_DateChanged);
            // 
            // scheduleButton
            // 
            this.scheduleButton.Location = new System.Drawing.Point(428, 213);
            this.scheduleButton.Name = "scheduleButton";
            this.scheduleButton.Size = new System.Drawing.Size(115, 26);
            this.scheduleButton.TabIndex = 7;
            this.scheduleButton.Text = "Schedule";
            this.scheduleButton.UseVisualStyleBackColor = true;
            this.scheduleButton.Click += new System.EventHandler(this.scheduleButton_Click);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(25, 436);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(81, 45);
            this.editButton.TabIndex = 11;
            this.editButton.Text = "Edit Appointment";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // delButton
            // 
            this.delButton.Location = new System.Drawing.Point(152, 436);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(81, 45);
            this.delButton.TabIndex = 12;
            this.delButton.Text = "Delete Appointment";
            this.delButton.UseVisualStyleBackColor = true;
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // appointmentListBox
            // 
            this.appointmentListBox.FormattingEnabled = true;
            this.appointmentListBox.Location = new System.Drawing.Point(31, 263);
            this.appointmentListBox.Name = "appointmentListBox";
            this.appointmentListBox.Size = new System.Drawing.Size(1037, 134);
            this.appointmentListBox.TabIndex = 13;
            // 
            // petsComboBox
            // 
            this.petsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.petsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.petsComboBox.FormattingEnabled = true;
            this.petsComboBox.Location = new System.Drawing.Point(181, 113);
            this.petsComboBox.Name = "petsComboBox";
            this.petsComboBox.Size = new System.Drawing.Size(362, 21);
            this.petsComboBox.TabIndex = 5;
            // 
            // appointmentTypeComboBox
            // 
            this.appointmentTypeComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.appointmentTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appointmentTypeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.appointmentTypeComboBox.FormattingEnabled = true;
            this.appointmentTypeComboBox.Items.AddRange(new object[] {
            "Surgery",
            "Dentistry",
            "Shear",
            "Vaccination",
            "Euthanasia"});
            this.appointmentTypeComboBox.Location = new System.Drawing.Point(181, 149);
            this.appointmentTypeComboBox.Name = "appointmentTypeComboBox";
            this.appointmentTypeComboBox.Size = new System.Drawing.Size(362, 21);
            this.appointmentTypeComboBox.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Choose the Appointment type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(651, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Hours Appointment";
            // 
            // hoursNumericUpDown
            // 
            this.hoursNumericUpDown.Location = new System.Drawing.Point(811, 218);
            this.hoursNumericUpDown.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.hoursNumericUpDown.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.hoursNumericUpDown.Name = "hoursNumericUpDown";
            this.hoursNumericUpDown.Size = new System.Drawing.Size(70, 20);
            this.hoursNumericUpDown.TabIndex = 18;
            this.hoursNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(993, 470);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 28;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // SchedulingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 529);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.hoursNumericUpDown);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.appointmentTypeComboBox);
            this.Controls.Add(this.appointmentListBox);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.scheduleButton);
            this.Controls.Add(this.monthCalendar);
            this.Controls.Add(this.petsComboBox);
            this.Controls.Add(this.clientsComboBox);
            this.Controls.Add(this.doctorsComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SchedulingForm";
            this.Text = "Scheduling";
            ((System.ComponentModel.ISupportInitialize)(this.hoursNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox doctorsComboBox;
        private System.Windows.Forms.ComboBox clientsComboBox;
        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.Button scheduleButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button delButton;
        private System.Windows.Forms.ListBox appointmentListBox;
        private System.Windows.Forms.ComboBox petsComboBox;
        private System.Windows.Forms.ComboBox appointmentTypeComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown hoursNumericUpDown;
        private System.Windows.Forms.Button backButton;
    }
}