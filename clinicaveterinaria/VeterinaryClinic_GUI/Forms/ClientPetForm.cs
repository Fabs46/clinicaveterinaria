﻿namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class ClientPetForm : Form
    {

        Clients Clients;

        ClinicForm ClinicForm;

        Pets Pets;

        Client Client;

        Appointments Appointments;

        public ClientPetForm(Clients Clients, ClinicForm ClinicForm, Pets Pets, Client Client, Appointments Appointments)
        {
            InitializeComponent();
            this.Clients = Clients;
            this.ClinicForm = ClinicForm;
            this.Pets = Pets;
            this.Client = Client;
            this.Appointments = Appointments;

            InitList();
        }


        private void ClientFormButton_Click(object sender, EventArgs e)
        {
            ClientForm addClientForm = new ClientForm(ClinicForm, Clients, this, Pets, Client);
            addClientForm.ShowDialog();
            this.Show();
        }

        private void editClientButton_Click(object sender, EventArgs e)
        {
            if (ClientsListBox.SelectedItem == null)
            {
                MessageBox.Show("Please select Client ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            EditClientForm EditClientForm = new EditClientForm(this, Clients, ClinicForm, Clients.ToEditLine((Client)ClientsListBox.SelectedItem));

            EditClientForm.ShowDialog();
            this.Show();
        }

        private void deleteClientButton_Click(object sender, EventArgs e)
        {

            Client clientRemove = Clients.DeleteClient((Client)ClientsListBox.SelectedItem);//Object created to connect to the Method in Controller

            if (ClientsListBox.SelectedItem == null)
            {
                MessageBox.Show("Please select Client ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Client.IdClient == clientRemove.IdClient)
                {
                    MessageBox.Show("Appointments pending on this Client", "Impossible to Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            if (clientRemove != null)
            {
                DialogResult answer;
                answer = MessageBox.Show($"Do you really want to delete {clientRemove.ClientInfo}",
                    "Delete",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question);


                if (DialogResult.OK == answer)
                {
         
                    Clients.ListClients.Remove(clientRemove);

                    Clients.SaveInfo();
                    Pets.SaveInfo(Clients.ListClients);

                    InitList();
                }

            }
        }

        private void petsFormButton_Click(object sender, EventArgs e)
        {
            if (ClientsListBox.SelectedItem != null)
            {

                PetForm PetForm = new PetForm(Pets, ClinicForm, Clients, this, (Client)ClientsListBox.SelectedItem);
                PetForm.ShowDialog();
                this.Show();
            }

            else
            {
                if (Clients.idClient == 1)
                {
                    DialogResult answer;
                    answer = MessageBox.Show($"Add Client and Select please", "Atention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    DialogResult answer;
                    answer = MessageBox.Show($"Select Client please", "Atention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void editPetButton_Click(object sender, EventArgs e)
        {

            if (PetListBox.SelectedItem == null)
            {
                MessageBox.Show("Please select the Owner and the Pet ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Pet PetToEdit;
            PetToEdit = new Pet();
            PetToEdit = (Pet)PetListBox.SelectedItem;

            Client ClientSelected;
            ClientSelected = (Client)ClientsListBox.SelectedItem;


            EditPetForm EditPetForm = new EditPetForm(this, Pets, ClinicForm, PetToEdit, ClientSelected, Clients/*Pets.ToEditLinePet((Animal)PetListBox.SelectedItem)*/);

            EditPetForm.ShowDialog();
            this.Show();
        }

        private void deletePetButton_Click_1(object sender, EventArgs e)
        {
            Client Client = (Client)ClientsListBox.SelectedItem;//Client needs to be instantiate 

            Pet petRemove = Pets.DeletePet((Pet)PetListBox.SelectedItem, Client);//Object created to connect to the Method in Controller

            if (PetListBox.SelectedItem == null)
            {
                MessageBox.Show("Please select the Owner and the Pet ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Pet.NamePet == petRemove.NamePet)

                {
                    MessageBox.Show("Appointments pending on this Pet", "Impossible to Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (petRemove != null)
            {
                DialogResult answer;
                answer = MessageBox.Show($"Do you really want to delete {petRemove.PetInfo}",
                    "Delete",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question);

                if (DialogResult.OK == answer)
                {
                    if (Client.Pets.Count == 1)
                    {
                        MessageBox.Show($"Is the Last Pet on this Client's list, client will be removed", "Warning");
                        Clients.ListClients.Remove(Client);
                        Clients.SaveInfo();
                        InitList();
                    }

                    Client.Pets.Remove(petRemove);
                    Pets.DeletePet(petRemove, Client);
                    Pets.SaveInfo(Clients.ListClients);

                    ClientsListBox.DataSource = null;
                    ClientsListBox.DataSource = Clients.ListClients;// getting ListClients from controller
                    ClientsListBox.DisplayMember = "ClientInfo";

                    PetListBox.DataSource = Client.Pets;
                    PetListBox.DisplayMember = "PetInfo";
                }
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Loads needed lists
        /// </summary>
        public void InitList()
        {
            //Needs to be on this order to be possible to show the Pet from the Selected Client
            ClientsListBox.DataSource = null;
            ClientsListBox.DataSource = Clients.ListClients;// getting ListClients from controller
            ClientsListBox.DisplayMember = "ClientInfo";
            ClientsListBox.SelectedItem = null;

            PetListBox.DataSource = null;
            PetListBox.DisplayMember = "PetInfo";
        }

        /// <summary>
        /// Event to display Pet depending on the the Client Selected
        /// </summary>
        private void ClientsListBox_SelectedValueChanged(object sender, EventArgs e)
        {

            if (ClientsListBox.SelectedItem != null)
            {
                Client selectedClient = (Client)ClientsListBox.SelectedItem;

                PetListBox.DataSource = null;
                PetListBox.DataSource = selectedClient.Pets;
            }

            PetListBox.DisplayMember = "PetInfo";

        }


    }
}

