﻿namespace LibraryVeterinaryClinic
{
 
    /// <summary>
    ///  Model Animal
    /// </summary>
    public class Pet
    {
        public int IdOwener { get; set; }

        public int IdPet { get; set; }

        public string NamePet { get; set; }

        public string AnimalType { get; set; }

        public int Age { get; set; }

        public double Weight { get; set; }

        public Client Client { get; set; }

        public string PetInfo
        {
            get
            {
                return $"{IdPet} -> {AnimalType} - {NamePet} Age:{Age} Weight:{Weight}";
            }
        }
    }
}
