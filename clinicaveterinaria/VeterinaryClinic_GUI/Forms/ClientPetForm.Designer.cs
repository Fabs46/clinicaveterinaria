﻿namespace VeterinaryClinic_GUI.Forms
{
    partial class ClientPetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.deleteClientButton = new System.Windows.Forms.Button();
            this.editClientButton = new System.Windows.Forms.Button();
            this.ClientsListBox = new System.Windows.Forms.ListBox();
            this.petsFormButton = new System.Windows.Forms.Button();
            this.ClientFormButton = new System.Windows.Forms.Button();
            this.PetListBox = new System.Windows.Forms.ListBox();
            this.deletePetButton = new System.Windows.Forms.Button();
            this.editPetButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(266, 417);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(63, 21);
            this.backButton.TabIndex = 27;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // deleteClientButton
            // 
            this.deleteClientButton.Location = new System.Drawing.Point(34, 134);
            this.deleteClientButton.Name = "deleteClientButton";
            this.deleteClientButton.Size = new System.Drawing.Size(93, 47);
            this.deleteClientButton.TabIndex = 24;
            this.deleteClientButton.Text = "Delete Selected Client";
            this.deleteClientButton.UseVisualStyleBackColor = true;
            this.deleteClientButton.Click += new System.EventHandler(this.deleteClientButton_Click);
            // 
            // editClientButton
            // 
            this.editClientButton.Location = new System.Drawing.Point(34, 78);
            this.editClientButton.Name = "editClientButton";
            this.editClientButton.Size = new System.Drawing.Size(93, 47);
            this.editClientButton.TabIndex = 23;
            this.editClientButton.Text = "Edit Selected Client";
            this.editClientButton.UseVisualStyleBackColor = true;
            this.editClientButton.Click += new System.EventHandler(this.editClientButton_Click);
            // 
            // ClientsListBox
            // 
            this.ClientsListBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientsListBox.FormattingEnabled = true;
            this.ClientsListBox.Location = new System.Drawing.Point(151, 22);
            this.ClientsListBox.Name = "ClientsListBox";
            this.ClientsListBox.Size = new System.Drawing.Size(178, 160);
            this.ClientsListBox.TabIndex = 22;
            this.ClientsListBox.SelectedValueChanged += new System.EventHandler(this.ClientsListBox_SelectedValueChanged);
            // 
            // petsFormButton
            // 
            this.petsFormButton.Location = new System.Drawing.Point(236, 231);
            this.petsFormButton.Name = "petsFormButton";
            this.petsFormButton.Size = new System.Drawing.Size(93, 47);
            this.petsFormButton.TabIndex = 29;
            this.petsFormButton.Text = "Add Your  Pet";
            this.petsFormButton.UseVisualStyleBackColor = true;
            this.petsFormButton.Click += new System.EventHandler(this.petsFormButton_Click);
            // 
            // ClientFormButton
            // 
            this.ClientFormButton.Location = new System.Drawing.Point(34, 22);
            this.ClientFormButton.Name = "ClientFormButton";
            this.ClientFormButton.Size = new System.Drawing.Size(93, 47);
            this.ClientFormButton.TabIndex = 30;
            this.ClientFormButton.Text = "Add Client";
            this.ClientFormButton.UseVisualStyleBackColor = true;
            this.ClientFormButton.Click += new System.EventHandler(this.ClientFormButton_Click);
            // 
            // PetListBox
            // 
            this.PetListBox.FormattingEnabled = true;
            this.PetListBox.Location = new System.Drawing.Point(34, 231);
            this.PetListBox.Name = "PetListBox";
            this.PetListBox.Size = new System.Drawing.Size(178, 160);
            this.PetListBox.TabIndex = 31;
            // 
            // deletePetButton
            // 
            this.deletePetButton.Location = new System.Drawing.Point(236, 343);
            this.deletePetButton.Name = "deletePetButton";
            this.deletePetButton.Size = new System.Drawing.Size(93, 47);
            this.deletePetButton.TabIndex = 33;
            this.deletePetButton.Text = "Delete Selected Pet";
            this.deletePetButton.UseVisualStyleBackColor = true;
            this.deletePetButton.Click += new System.EventHandler(this.deletePetButton_Click_1);
            // 
            // editPetButton
            // 
            this.editPetButton.Location = new System.Drawing.Point(236, 287);
            this.editPetButton.Name = "editPetButton";
            this.editPetButton.Size = new System.Drawing.Size(93, 47);
            this.editPetButton.TabIndex = 32;
            this.editPetButton.Text = "Edit Selected Pet";
            this.editPetButton.UseVisualStyleBackColor = true;
            this.editPetButton.Click += new System.EventHandler(this.editPetButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 15);
            this.label1.TabIndex = 35;
            this.label1.Text = "First Select the Client than the Pet";
            // 
            // ClientPetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(406, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.deletePetButton);
            this.Controls.Add(this.editPetButton);
            this.Controls.Add(this.PetListBox);
            this.Controls.Add(this.ClientFormButton);
            this.Controls.Add(this.petsFormButton);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.deleteClientButton);
            this.Controls.Add(this.editClientButton);
            this.Controls.Add(this.ClientsListBox);
            this.Name = "ClientPetForm";
            this.Text = "ClientForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button deleteClientButton;
        private System.Windows.Forms.Button editClientButton;
        private System.Windows.Forms.ListBox ClientsListBox;
        private System.Windows.Forms.Button petsFormButton;
        private System.Windows.Forms.Button ClientFormButton;
        private System.Windows.Forms.ListBox PetListBox;
        private System.Windows.Forms.Button deletePetButton;
        private System.Windows.Forms.Button editPetButton;
        private System.Windows.Forms.Label label1;
    }
}