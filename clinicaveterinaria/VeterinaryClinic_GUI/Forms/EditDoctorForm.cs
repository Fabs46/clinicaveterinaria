﻿namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class EditDoctorForm : Form
    {
        Doctor Doctor;

        Doctors Doctors;

        DoctorForm DoctorForm;

        ClinicForm ClinicForm;

  
        public EditDoctorForm(DoctorForm form, Doctors Doctors, ClinicForm ClinicForm, Doctor Doctor)
        {
            InitializeComponent();
            this.Doctor = Doctor;
            this.Doctors = Doctors;
            this.DoctorForm = form;
            this.ClinicForm = ClinicForm;

            InitInfo();
        }

        private void doctorSaveButton_Click(object sender, EventArgs e)
        {

            if (ValidationForm())
            {

                Doctors.Edition(Doctor, doctorNameTextBox.Text, doctorSurnameTextBox.Text, doctorRoomTextBox.Text, 
                    Convert.ToInt16(endShiftNumericUpDown.Value),
                    Convert.ToInt16(startShiftNumericUpDown.Value));//receiving the edition from the controller
                Doctors.SaveInfo();
                DoctorForm.InitList();

                this.Close();
            }
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void InitInfo()
        {
            doctorIdTextBox.Text = Doctor.IdDoctor.ToString();
            doctorNameTextBox.Text = Doctor.NameDoctor;
            doctorSurnameTextBox.Text = Doctor.SurnameDoctor;
            doctorRoomTextBox.Text = Doctor.Room;
        }


        /// <summary>
        /// Validation method
        /// </summary>
        /// <returns>output bool if true keeps doing the task and just returns true , if false stops the task and display the Error Messages</returns>
        private bool ValidationForm()
        {
            bool output = true;

            if (startShiftNumericUpDown.Value >= endShiftNumericUpDown.Value)
            {
                MessageBox.Show("Start Shift is transpassing the limit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (endShiftNumericUpDown.Value - startShiftNumericUpDown.Value < 4)
            {
                MessageBox.Show("Less than 4 hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (endShiftNumericUpDown.Value - startShiftNumericUpDown.Value > 9)
            {
                MessageBox.Show("More than 9 hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(doctorNameTextBox.Text))
            {
                MessageBox.Show("Insert Doctor's name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(doctorSurnameTextBox.Text))
            {
                MessageBox.Show("Insert Doctor's surname", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(doctorRoomTextBox.Text))
            {
                MessageBox.Show("Insert Doctor's room", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            foreach (Doctor doctor in Doctors.ListDoctors)
            {
                if (doctorRoomTextBox.Text == doctor.Room && doctor.IdDoctor == Convert.ToInt16(doctorIdTextBox.Text))
                {
                    return output;
                }
                if (doctorRoomTextBox.Text == doctor.Room && doctor.IdDoctor != Convert.ToInt16(doctorIdTextBox.Text))
                {
                    MessageBox.Show("Doctor's room ocupied", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    output = false;
                    return output;
                }
            }

            return output;
        }
    }
}
