﻿namespace VeterinaryClinic_GUI.Forms
{
    partial class EditDoctorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.doctorSaveButton = new System.Windows.Forms.Button();
            this.doctorSurnameTextBox = new System.Windows.Forms.TextBox();
            this.doctorNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorIdTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.endShiftNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.startShiftNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.doctorRoomTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.endShiftNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startShiftNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.SteelBlue;
            this.cancelButton.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.SystemColors.Control;
            this.cancelButton.Location = new System.Drawing.Point(302, 265);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 25);
            this.cancelButton.TabIndex = 24;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // doctorSaveButton
            // 
            this.doctorSaveButton.BackColor = System.Drawing.Color.SteelBlue;
            this.doctorSaveButton.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorSaveButton.ForeColor = System.Drawing.SystemColors.Control;
            this.doctorSaveButton.Location = new System.Drawing.Point(196, 265);
            this.doctorSaveButton.Name = "doctorSaveButton";
            this.doctorSaveButton.Size = new System.Drawing.Size(75, 25);
            this.doctorSaveButton.TabIndex = 23;
            this.doctorSaveButton.Text = "Save";
            this.doctorSaveButton.UseVisualStyleBackColor = false;
            this.doctorSaveButton.Click += new System.EventHandler(this.doctorSaveButton_Click);
            // 
            // doctorSurnameTextBox
            // 
            this.doctorSurnameTextBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorSurnameTextBox.Location = new System.Drawing.Point(129, 109);
            this.doctorSurnameTextBox.Name = "doctorSurnameTextBox";
            this.doctorSurnameTextBox.Size = new System.Drawing.Size(346, 22);
            this.doctorSurnameTextBox.TabIndex = 22;
            // 
            // doctorNameTextBox
            // 
            this.doctorNameTextBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorNameTextBox.Location = new System.Drawing.Point(129, 67);
            this.doctorNameTextBox.Name = "doctorNameTextBox";
            this.doctorNameTextBox.Size = new System.Drawing.Size(346, 22);
            this.doctorNameTextBox.TabIndex = 21;
            // 
            // doctorIdTextBox
            // 
            this.doctorIdTextBox.Enabled = false;
            this.doctorIdTextBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorIdTextBox.Location = new System.Drawing.Point(129, 25);
            this.doctorIdTextBox.Name = "doctorIdTextBox";
            this.doctorIdTextBox.ReadOnly = true;
            this.doctorIdTextBox.Size = new System.Drawing.Size(116, 22);
            this.doctorIdTextBox.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Doctor\'s Surname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Doctor\'s Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Doctor\'s ID";
            // 
            // endShiftNumericUpDown
            // 
            this.endShiftNumericUpDown.Location = new System.Drawing.Point(129, 220);
            this.endShiftNumericUpDown.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.endShiftNumericUpDown.Minimum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.endShiftNumericUpDown.Name = "endShiftNumericUpDown";
            this.endShiftNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.endShiftNumericUpDown.TabIndex = 31;
            this.endShiftNumericUpDown.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // startShiftNumericUpDown
            // 
            this.startShiftNumericUpDown.Location = new System.Drawing.Point(129, 186);
            this.startShiftNumericUpDown.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.startShiftNumericUpDown.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.startShiftNumericUpDown.Name = "startShiftNumericUpDown";
            this.startShiftNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.startShiftNumericUpDown.TabIndex = 30;
            this.startShiftNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Doctor\'s End Shift";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Doctor\'s Start Shift";
            // 
            // doctorRoomTextBox
            // 
            this.doctorRoomTextBox.Location = new System.Drawing.Point(129, 149);
            this.doctorRoomTextBox.Name = "doctorRoomTextBox";
            this.doctorRoomTextBox.Size = new System.Drawing.Size(122, 20);
            this.doctorRoomTextBox.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Doctor\'s Room";
            // 
            // EditDoctorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 362);
            this.Controls.Add(this.endShiftNumericUpDown);
            this.Controls.Add(this.startShiftNumericUpDown);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.doctorRoomTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.doctorSaveButton);
            this.Controls.Add(this.doctorSurnameTextBox);
            this.Controls.Add(this.doctorNameTextBox);
            this.Controls.Add(this.doctorIdTextBox);
            this.Name = "EditDoctorForm";
            this.Text = "EditDoctorForm";
            ((System.ComponentModel.ISupportInitialize)(this.endShiftNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startShiftNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button doctorSaveButton;
        private System.Windows.Forms.TextBox doctorSurnameTextBox;
        private System.Windows.Forms.TextBox doctorNameTextBox;
        private System.Windows.Forms.TextBox doctorIdTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown endShiftNumericUpDown;
        private System.Windows.Forms.NumericUpDown startShiftNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox doctorRoomTextBox;
        private System.Windows.Forms.Label label3;
    }
}