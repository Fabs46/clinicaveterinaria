﻿namespace LibraryVeterinaryClinic
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class Controller Clients
    /// </summary>
    public class Clients
    {
        //from the Model
        public Client newClient;

        public Pet newPet;

        //creating a new list Client to be used firstly on Method AddClient then onmore Methods and Forms
        public List<Client> ListClients = new List<Client>();

        public Client client;

        public int idClient = 1;//Client's ID starts on 1

        /// <summary>
        /// add a Client to the ListClients ant increments the ID 
        /// (mainly used on ClientForm and on ReadInfo())
        /// Saves into a txt File
        /// </summary>
        /// <param name="name">Receveing from the Form or the file</param>
        /// <param name="surname">Receveing from the Form or the file</param>
        /// <param name="adress">Receveing from the Form or the file</param>
        /// <param name="nif">Receveing from the Form or the file</param>
        /// <param name="phone">Receveing from the Form or the file</param>
        /// <param name="email">Receveing from the Form or the file</param>
        public void AddClient(string name, string surname, string adress, string nif, int phone, string email)
        {
            newClient = new Client// creates a new Object from the Model Client # Constructor
            {
                IdClient = idClient,
                NameClient = name,
                SurnameClient = surname,
                AdressClient = adress,
                NIF = nif,
                Email = email,
                Phone = phone,

            };

            ListClients.Add(newClient);// adding values into the list
            SaveInfo();

            idClient++;//id increment
        }

        /// <summary>
        /// Method to delete Client returning a Blank Client
        /// </summary>
        /// <param name="clientSelected">Receiving from Form the line Selected</param>
        /// <returns></returns>
        public Client DeleteClient(Client clientSelected)
        {
            Client clientBlank = null;

            if (clientSelected != null)
            {
                foreach (Client client in ListClients)
                {
                    if (clientSelected.IdClient == client.IdClient)
                    {
                        clientBlank = client;
                    }
                }
                return clientBlank;
            }
            return clientBlank;
        }

        /// <summary>
        ///Method to return the selected Client for Edition
        /// </summary>
        /// <param name="clientSelected">selectedLine</param>
        /// <returns></returns>
        public Client ToEditLine(Client clientSelected)
        {
            Client selected = null;

            if (clientSelected != null)
            {
                foreach (Client Client in ListClients)
                {
                    if (clientSelected.IdClient == Client.IdClient)
                    {
                        selected = Client;
                    }
                }
            }
            return selected;
        }

        /// <summary>
        /// Edits from the form EditClientAppointment geeting the selected Client
        /// </summary>
        /// <param name="Client">Receiving from Form the line Selected (Cast on Client Form)</param>
        /// <param name="name">Receiving from Form</param>
        /// <param name="surname">Receiving from Form</param>
        /// <param name="adress">Receiving from Form</param>
        /// <param name="nif">Receiving from Form</param>
        /// <param name="phone">Receiving from Form</param>
        /// <param name="email">Receiving from Form</param>
        public void Edition(Client Client, string name, string surname, string adress,string nif,int phone,string email)
        {
            Client.NameClient = name;
            Client.SurnameClient = surname;
            Client.AdressClient = adress;
            Client.NIF = nif;
            Client.Phone = phone;
            Client.Email = email;
        }

        /// <summary>
        /// Creates a File and adds lines sw
        /// </summary>
        public void SaveInfo()
        {
            string file = @"ClientInfo.txt";

            StreamWriter sw = new StreamWriter(file,false);


            if (!File.Exists(file))
            {
                sw = File.CreateText(file);
            }

            foreach (Client newClient in ListClients)
            {
                string line = $"{newClient.IdClient};{newClient.NameClient};{newClient.SurnameClient};{newClient.AdressClient};" +
                    $"{newClient.NIF};{newClient.Phone};{newClient.Email}";

                sw.WriteLine(line);

            }

            sw.Close();
        }

        /// <summary>
        /// Loads fields from a txt File (string[] fields) and uses the Method Add to add to the list 
        /// </summary>
        public void ReadInfo()
        {

            string file = @"ClientInfo.txt";

            StreamReader sr;

            if (File.Exists(file))
            {
                sr = File.OpenText(file);
                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    newClient = new Client();//needs to instance to create a new List newClient

                    string[] fields = new string[7];

                    fields = line.Split(';');//split fields

                    newClient.IdClient = Convert.ToInt16(fields[0]);
                    newClient.NameClient = (fields[1]);
                    newClient.SurnameClient = (fields[2]);
                    newClient.AdressClient = (fields[3]);
                    newClient.NIF = (fields[4]);
                    newClient.Phone = Convert.ToInt32((fields[5]));
                    newClient.Email = (fields[6]);


                    ListClients.Add(newClient);//Add an new Client object into the list

                    idClient = newClient.IdClient + 1;
                }
                sr.Close();
            }
        }

    }
}
