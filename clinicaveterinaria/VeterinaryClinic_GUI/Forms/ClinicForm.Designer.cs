﻿namespace VeterinaryClinic_GUI.Forms
{
    partial class ClinicForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.doctorsButton = new System.Windows.Forms.Button();
            this.clientButton = new System.Windows.Forms.Button();
            this.schedulingButton = new System.Windows.Forms.Button();
            this.MessagingButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // doctorsButton
            // 
            this.doctorsButton.BackColor = System.Drawing.Color.CadetBlue;
            this.doctorsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.doctorsButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorsButton.Location = new System.Drawing.Point(230, 37);
            this.doctorsButton.Name = "doctorsButton";
            this.doctorsButton.Size = new System.Drawing.Size(111, 28);
            this.doctorsButton.TabIndex = 0;
            this.doctorsButton.Text = "Doctors Regist";
            this.doctorsButton.UseVisualStyleBackColor = false;
            this.doctorsButton.Click += new System.EventHandler(this.doctorsButton_Click);
            // 
            // clientButton
            // 
            this.clientButton.BackColor = System.Drawing.Color.PaleGreen;
            this.clientButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.clientButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientButton.Location = new System.Drawing.Point(230, 91);
            this.clientButton.Name = "clientButton";
            this.clientButton.Size = new System.Drawing.Size(111, 28);
            this.clientButton.TabIndex = 4;
            this.clientButton.Text = "Clients Regist";
            this.clientButton.UseVisualStyleBackColor = false;
            this.clientButton.Click += new System.EventHandler(this.clientButton_Click);
            // 
            // schedulingButton
            // 
            this.schedulingButton.BackColor = System.Drawing.Color.DarkRed;
            this.schedulingButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.schedulingButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schedulingButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.schedulingButton.Location = new System.Drawing.Point(230, 147);
            this.schedulingButton.Name = "schedulingButton";
            this.schedulingButton.Size = new System.Drawing.Size(114, 26);
            this.schedulingButton.TabIndex = 7;
            this.schedulingButton.Text = "Scheduling";
            this.schedulingButton.UseVisualStyleBackColor = false;
            this.schedulingButton.Click += new System.EventHandler(this.schedulingButton_Click);
            // 
            // MessagingButton
            // 
            this.MessagingButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.MessagingButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.MessagingButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessagingButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.MessagingButton.Location = new System.Drawing.Point(230, 196);
            this.MessagingButton.Name = "MessagingButton";
            this.MessagingButton.Size = new System.Drawing.Size(111, 28);
            this.MessagingButton.TabIndex = 8;
            this.MessagingButton.Text = "Contact Client";
            this.MessagingButton.UseVisualStyleBackColor = false;
            this.MessagingButton.Click += new System.EventHandler(this.MessagingButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(310, 278);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Version 1.0.2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(310, 305);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Author: Fábio Sarreira";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(310, 331);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(61, 13);
            this.labelDate.TabIndex = 12;
            this.labelDate.Text = "labelDate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Doctors Menu ->";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(44, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Clients Menu ->";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(44, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Appointments Menu ->";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(44, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(163, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Communications Menu ->";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Location = new System.Drawing.Point(301, 262);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(162, 96);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.TabStop = false;
            this.richTextBox1.Text = "";
            // 
            // ClinicForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 380);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MessagingButton);
            this.Controls.Add(this.schedulingButton);
            this.Controls.Add(this.clientButton);
            this.Controls.Add(this.doctorsButton);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ClinicForm";
            this.Text = "ClinicForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClinicForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button doctorsButton;
        private System.Windows.Forms.Button clientButton;
        private System.Windows.Forms.Button schedulingButton;
        private System.Windows.Forms.Button MessagingButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}