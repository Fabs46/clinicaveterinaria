﻿namespace LibraryVeterinaryClinic
{
    using System.Collections.Generic;


    /// <summary>
    /// Model Client
    /// </summary>
    public class Client
    {
        public int IdClient { get; set; }

        public string NameClient { get; set; }

        public string SurnameClient { get; set; }

        public string AdressClient { get; set; }

        public string NIF { get; set; }

        public int Phone { get; set; }

        public string Email { get; set; }

        public List<Pet> Pets { get; set; } = new List<Pet>();



        public string ClientInfo
        {
            get
            {
                return $"{IdClient} - {NameClient} {SurnameClient} NIF: {NIF} Adress: {AdressClient} Phone: {Phone} Email: {Email}";

            }
        }

    }
}
