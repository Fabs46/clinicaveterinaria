﻿namespace LibraryVeterinaryClinic
{
    using System.Collections.Generic;

    /// <summary>
    /// Model Appointment
    /// </summary>
    public class Appointment
    {
        public int IdAppointment { get; set; }

        public Doctor Doctor { get; set; }

        public Client Client { get; set; }

        public Pet Pet { get; set; }

        public string AppointmentType { get; set; }

        public int AppointmentHour {get; set;}

        public string Date { get; set; }


        public string AppointmentInfo
        {

            get
            {
                return $" {AppointmentType} Appointment Nr {IdAppointment} at {AppointmentHour} o'clock on {Date}" +
                    $" with Doctor {Doctor.NameDoctor} {Doctor.SurnameDoctor} |" +
                    $" Room: {Doctor.Room} DS({Doctor.StartShift} - {Doctor.EndShift})" +
                    $" for the Client Nr-> {Client.IdClient} {Client.NameClient} {Client.SurnameClient} " +
                    $" and Pet Nr-> {Pet.IdPet} {Pet.NamePet} Type: {Pet.AnimalType} Age: {Pet.Age} Weight: {Pet.Weight}";
 

            }
        }

    }
}
