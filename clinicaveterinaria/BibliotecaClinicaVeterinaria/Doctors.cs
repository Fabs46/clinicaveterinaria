﻿namespace LibraryVeterinaryClinic
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class Controller Doctors
    /// </summary>
    public class Doctors
    {

        public Doctor newDoctor;

        //creating a new list Doctor to be used firstly on Method AddDoctor then onmore Methods and Forms
        public List<Doctor> ListDoctors = new List<Doctor>();

        public int idDoctor = 1;//Doctor's ID starts on 1


        /// <summary>
        /// add a Doctor to the ListDoctors ant increments the ID 
        /// (mainly used on DoctorForm and on ReadInfo()))
        /// Saves into a txt File
        /// </summary>
        /// <param name="name">Receveing from the Form or the file</param>
        /// <param name="surname">Receveing from the Form</param>
        /// <param name="room">Receveing from the Form</param>
        /// <param name="startShift">Receveing from the Form</param>
        /// <param name="endShift">Receveing from the Form</param>
        public void AddDoctor(string name, string surname, string room, int startShift, int endShift)
        {
            newDoctor = new Doctor// creates a new Object from the Model Doctor # Constructor
            {
                IdDoctor = idDoctor,
                NameDoctor = name,
                SurnameDoctor = surname,
                Room = room,
                StartShift = startShift,
                EndShift = endShift,
            };

            ListDoctors.Add(newDoctor);// adding values into the list
            SaveInfo();

            idDoctor++;//id increment
        }

        /// <summary>
        /// Method to delete Doctor returning a Blank Doctor
        /// </summary>
        /// <param name="doctorSelected">Receiving from Form the line Selected</param>
        /// <returns></returns>
        public Doctor DeleteDoctor(Doctor doctorSelected)
        {

            Doctor doctorBlank = null;

            if (doctorSelected != null)
            {
                foreach (Doctor doctor in ListDoctors)
                {
                    if (doctorSelected.IdDoctor == doctor.IdDoctor)
                    {

                        doctorBlank = doctor; //gets the doctor selected
                    }
                }
                
                return doctorBlank;
            }
 
            return doctorBlank;
        }

        /// <summary>
        /// Method to return the selected Doctor for Edition
        /// </summary>
        /// <param name="doctorSelected">Receiving from Form the line Selected</param>
        /// <returns></returns>
        public Doctor ToEditLine(Doctor doctorSelected)
        {
            Doctor selected = null;

            if (doctorSelected != null)
            {
                foreach (Doctor doctor in ListDoctors)
                {
                    if (doctorSelected.IdDoctor == doctor.IdDoctor)
                    {
                        selected = doctor; //gets the doctor selected
                    }
                }
            }
            return selected;
        }


        /// <summary>
        /// Edits from the form EditDoctorAppointment geeting the selected Doctor
        /// </summary>
        /// <param name="doctor">Receiving from Form the line Selected (Cast on Doctor Form)</param>
        /// <param name="name">Receiving from Form </param>
        /// <param name="surname">Receiving from Form</param>
        /// <param name="room">Receiving from Form the </param>
        /// <param name="startShift">Receiving from Form </param>
        /// <param name="endShift">Receiving from Form </param>
        public void Edition(Doctor doctor,string name,string surname, string room, int startShift,int endShift )
        {
            doctor.NameDoctor = name;
            doctor.SurnameDoctor = surname;
            doctor.Room = room;
            doctor.StartShift = startShift;
            doctor.EndShift = endShift;
        }


        /// <summary>
        /// Creates a File and adds lines sw
        /// </summary>
        public void SaveInfo()
        {
            string file = @"DoctorInfo.txt";

            StreamWriter sw = new StreamWriter(file, false);

            if (!File.Exists(file))
            {
                sw = File.CreateText(file);
            }

            foreach (Doctor newDoctor in ListDoctors)
            {
                string line = $"{newDoctor.IdDoctor};{newDoctor.NameDoctor};{newDoctor.SurnameDoctor};{newDoctor.Room};{newDoctor.StartShift};{newDoctor.EndShift}";
                sw.WriteLine(line);
            }

            sw.Close();
        }

        /// <summary>
        /// Loads fields from a txt File (string[] fields) and uses the Method Add to add to the list 
        /// </summary>
        public void ReadInfo()
        {
            string file = @"DoctorInfo.txt";

            StreamReader sr;

            if (File.Exists(file))
            {
                sr = File.OpenText(file);
                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    newDoctor = new Doctor();//needs to instance newDoctor

                    string[] fields = new string[6];

                    fields = line.Split(';');//split fields
                    
                    newDoctor.IdDoctor = Convert.ToInt16(fields[0]);
                    newDoctor.NameDoctor = (fields[1]);
                    newDoctor.SurnameDoctor = (fields[2]);
                    newDoctor.Room = (fields[3]);
                    newDoctor.StartShift = Convert.ToInt16(fields[4]);
                    newDoctor.EndShift = Convert.ToInt16(fields[5]);

                    ListDoctors.Add(newDoctor);//Add an new doctor object into the list

                    idDoctor = newDoctor.IdDoctor + 1;
                }
                sr.Close();
            }
        }

    }
}
