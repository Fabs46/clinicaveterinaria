﻿namespace LibraryVeterinaryClinic
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class Controller Pets
    /// </summary>
    public class Pets
    {
        public Pet newPet;

        int idPet = 1; //ID starts on 1

        /// <summary>
        /// add a Pet to the ListPets ant increments the ID 
        /// (mainly used on PetForm and on ReadInfo())
        /// Saves into a txt File
        /// </summary>
        /// <param name="animalKind">Receveing from the Form or the file</param>
        /// <param name="name">Receveing from the Form or the file</param>
        /// <param name="age">Receveing from the Form or the file</param>
        /// <param name="weight">Receveing from the Form or the file</param>
        /// <param name="client">Receveing from the Form or the file</param>
        /// <param name="ClientControler">Receveing from the Form or the file</param>
        public void AddPet(string animalKind, string name, int age, double weight, Client client,Clients ClientControler)
        {
            newPet = new Pet
            {
                Client = client,
                IdOwener = client.IdClient,

                IdPet = idPet,
                AnimalType = animalKind,
                NamePet = name,
                Age = age,
                Weight = weight,

            };

            client.Pets.Add(newPet);// adding values into the list, Pets inside of the model class Client 

            ClientControler.SaveInfo();
            SaveInfo(ClientControler.ListClients);//Receiving List Clients

            idPet++;//id increment
        }

        /// <summary>
        /// Method to delete Pet returning a Blank Pet
        /// </summary>
        /// <param name="petSelected">Receiving from Form the line Selected</param>
        /// <returns></returns>
        public Pet DeletePet(Pet petSelected, Client client)
        {
            Pet PetBlank = null;

            if (petSelected != null)
            {
                foreach (Pet pet in client.Pets)
                {
                    if (petSelected.IdPet == pet.IdPet)
                    {
                        PetBlank = pet;
                    }
                }
                return PetBlank;
            }
            return PetBlank;
        }

        /// <summary>
        /// Method to return the selected Pet for Edition
        /// </summary>
        /// <param name="petSelected">selectedLine</param>
        /// <returns></returns>
        public Pet ToEditLinePet(Pet petSelected, Client client)
        {
            Pet selected = null;

            if (petSelected != null)
            {
                foreach (Pet Pet in client.Pets)
                {
                    if (petSelected.IdPet == Pet.IdPet)
                    {
                        selected = Pet;

                    }
                }
            }
            return selected;
        }

        /// <summary>
        /// Edits the received List(selected the Line) and changes from EditForm
        /// </summary>
        /// <param name="Animal">Receiving from Form</param>
        /// <param name="name">Receiving from Form</param>
        /// <param name="age">Receiving from Form</param>
        /// <param name="weight">Receiving from Form</param>
        public void EditionPet(Pet Animal, string name, string age, string weight)
        {
            Animal.NamePet = name;
            Animal.Age = Convert.ToInt16(age);
            Animal.Weight = Convert.ToDouble(weight);

        }

        /// <summary>
        /// Creates a File and adds lines sw
        /// </summary>
        /// <param name="Clients">receives the List Clients "foreaching" it and Saving Pets for each Client</param>
        public void SaveInfo(List<Client> Clients)
        {
            string file = @"PetInfo.txt";

            StreamWriter sw = new StreamWriter(file, false);

            if (!File.Exists(file))
            {
                sw = File.CreateText(file);
            }

            foreach (Client client in Clients)
            {
                foreach (Pet pet in client.Pets)
                {

                    string line = $"{pet.IdPet};{pet.NamePet};{pet.AnimalType};{pet.Age.ToString()};{pet.Weight.ToString()};{pet.IdOwener.ToString()}";

                    sw.WriteLine(line);
                }
            }

            sw.Close();
        }

        /// <summary>
        /// Loads fields from a txt File (string[] fields) and uses the Method Add to add to the list 
        /// </summary>
        /// <param name="clients">Calls the Controller making a foreach and creates a Pet List into each Client</param>
        public void ReadInfo(Clients clients)
        {

            string file = @"PetInfo.txt";

            StreamReader sr;

            if (File.Exists(file))
            {
                sr = File.OpenText(file);
                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    newPet = new Pet();//needs to instance 

                    string[] fields = new string[6];

                    fields = line.Split(';');//split fields

                    newPet.IdPet = Convert.ToInt16(fields[0]);
                    newPet.NamePet = (fields[1]);
                    newPet.AnimalType = (fields[2]);
                    newPet.Age = Convert.ToInt16(fields[3]);
                    newPet.Weight = Convert.ToDouble(fields[4]);
                    newPet.IdOwener = Convert.ToInt16(fields[5]);

                    //find list of clients
                    foreach (var client in clients.ListClients)
                    {
                        //search id owner pet if is the same adds to the list on client pets
                        if (newPet.IdOwener == client.IdClient)
                        {
                            client.Pets.Add(newPet);
                        }
                    }

                    idPet = newPet.IdPet + 1;
                }

                sr.Close();
            }
        }

    }
}
