﻿namespace VeterinaryClinic_GUI.Forms
{
    partial class EditAppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.appointmentTypeComboBox = new System.Windows.Forms.ComboBox();
            this.petsComboBox = new System.Windows.Forms.ComboBox();
            this.clientsComboBox = new System.Windows.Forms.ComboBox();
            this.doctorsComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label4 = new System.Windows.Forms.Label();
            this.appointmentIdTextBox = new System.Windows.Forms.TextBox();
            this.hoursNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.hoursNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Choose the Appointment type";
            // 
            // appointmentTypeComboBox
            // 
            this.appointmentTypeComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.appointmentTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appointmentTypeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.appointmentTypeComboBox.FormattingEnabled = true;
            this.appointmentTypeComboBox.Items.AddRange(new object[] {
            "Surgery",
            "Dentistry",
            "Shear",
            "Vaccination",
            "Euthanasia"});
            this.appointmentTypeComboBox.Location = new System.Drawing.Point(183, 177);
            this.appointmentTypeComboBox.Name = "appointmentTypeComboBox";
            this.appointmentTypeComboBox.Size = new System.Drawing.Size(303, 21);
            this.appointmentTypeComboBox.TabIndex = 22;
            // 
            // petsComboBox
            // 
            this.petsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.petsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.petsComboBox.FormattingEnabled = true;
            this.petsComboBox.Location = new System.Drawing.Point(183, 141);
            this.petsComboBox.Name = "petsComboBox";
            this.petsComboBox.Size = new System.Drawing.Size(303, 21);
            this.petsComboBox.TabIndex = 21;
            // 
            // clientsComboBox
            // 
            this.clientsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clientsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clientsComboBox.FormattingEnabled = true;
            this.clientsComboBox.Location = new System.Drawing.Point(183, 109);
            this.clientsComboBox.Name = "clientsComboBox";
            this.clientsComboBox.Size = new System.Drawing.Size(303, 21);
            this.clientsComboBox.TabIndex = 20;
            this.clientsComboBox.SelectedValueChanged += new System.EventHandler(this.clientsComboBox_SelectedValueChanged);
            // 
            // doctorsComboBox
            // 
            this.doctorsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorsComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.doctorsComboBox.FormattingEnabled = true;
            this.doctorsComboBox.Location = new System.Drawing.Point(183, 76);
            this.doctorsComboBox.Name = "doctorsComboBox";
            this.doctorsComboBox.Size = new System.Drawing.Size(303, 21);
            this.doctorsComboBox.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Choose Client\'s Pet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Choose Client";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Choose Doctor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(514, 232);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Hours Appointment";
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Brown;
            this.cancelButton.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.SystemColors.Control;
            this.cancelButton.Location = new System.Drawing.Point(215, 265);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 25);
            this.cancelButton.TabIndex = 31;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.Brown;
            this.saveButton.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.SystemColors.Control;
            this.saveButton.Location = new System.Drawing.Point(92, 265);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 25);
            this.saveButton.TabIndex = 30;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // monthCalendar
            // 
            this.monthCalendar.Location = new System.Drawing.Point(517, 36);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 32;
            this.monthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar_DateChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Appointment\'s ID";
            // 
            // appointmentIdTextBox
            // 
            this.appointmentIdTextBox.Enabled = false;
            this.appointmentIdTextBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appointmentIdTextBox.Location = new System.Drawing.Point(139, 26);
            this.appointmentIdTextBox.Name = "appointmentIdTextBox";
            this.appointmentIdTextBox.ReadOnly = true;
            this.appointmentIdTextBox.Size = new System.Drawing.Size(116, 22);
            this.appointmentIdTextBox.TabIndex = 33;
            // 
            // hoursNumericUpDown
            // 
            this.hoursNumericUpDown.Location = new System.Drawing.Point(674, 225);
            this.hoursNumericUpDown.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.hoursNumericUpDown.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.hoursNumericUpDown.Name = "hoursNumericUpDown";
            this.hoursNumericUpDown.Size = new System.Drawing.Size(70, 20);
            this.hoursNumericUpDown.TabIndex = 37;
            this.hoursNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(669, 328);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 38;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // EditAppointmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.hoursNumericUpDown);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.appointmentIdTextBox);
            this.Controls.Add(this.monthCalendar);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.appointmentTypeComboBox);
            this.Controls.Add(this.petsComboBox);
            this.Controls.Add(this.clientsComboBox);
            this.Controls.Add(this.doctorsComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EditAppointmentForm";
            this.Text = "EditAppointmentForm";
            ((System.ComponentModel.ISupportInitialize)(this.hoursNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox appointmentTypeComboBox;
        private System.Windows.Forms.ComboBox petsComboBox;
        private System.Windows.Forms.ComboBox clientsComboBox;
        private System.Windows.Forms.ComboBox doctorsComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox appointmentIdTextBox;
        private System.Windows.Forms.NumericUpDown hoursNumericUpDown;
        private System.Windows.Forms.Button backButton;
    }
}