﻿
namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class ClientForm : Form
    {

        Clients Clients;

        ClinicForm ClinicForm;

        ClientPetForm ClientPetForm;

        Pets Pets;

        Client Client;

        public ClientForm(ClinicForm ClinicForm, Clients Clients, ClientPetForm ClientPetForm,Pets Pets, Client Client)
        {
            this.ClientPetForm = ClientPetForm;
            this.ClinicForm = ClinicForm;
            this.Clients = Clients;
            this.Pets = Pets;
            this.Client = Client;

            InitializeComponent();
            InitList();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Do you want to cancel the operation?", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            clientNameTextBox.Text = string.Empty;
            clientSurnameTextBox.Text = string.Empty;
            nifMaskedTextBox.Text = string.Empty;
            adressTextBox.Text = string.Empty;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (ValidationForm())
            {
                Clients.AddClient(clientNameTextBox.Text, clientSurnameTextBox.Text, adressTextBox.Text, nifMaskedTextBox.Text, Convert.ToInt32(phoneMaskedTextBox.Text),emailTextBox.Text);

                ClientPetForm.InitList();
                InitList();// from this form
                PetForm PetForm = new PetForm(Pets ,ClinicForm,Clients,ClientPetForm,Clients.newClient);
                PetForm.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Fill correctly the fields and try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }



            clientNameTextBox.Text = string.Empty;
            clientSurnameTextBox.Text = string.Empty;
            nifMaskedTextBox.Text = string.Empty;
            adressTextBox.Text = string.Empty;
            phoneMaskedTextBox.Text = string.Empty;
            emailTextBox.Text = string.Empty;
        }

        /// <summary>
        /// Shows the list
        /// </summary>
        public void InitList()
        {
            ClientsListBox.DataSource = null;
            ClientsListBox.DataSource = Clients.ListClients;// getting ListClients from controller
            ClientsListBox.DisplayMember = "ClientInfo";
        }

        private bool ValidationForm()
        {
            bool output = true;

            if (String.IsNullOrEmpty(clientNameTextBox.Text))
            {
                MessageBox.Show("Insert Client's name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(clientSurnameTextBox.Text))
            {
                MessageBox.Show("Insert Client's surname", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(nifMaskedTextBox.Text))
            {
                MessageBox.Show("Insert Client's NIF", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(phoneMaskedTextBox.Text))
            {
                MessageBox.Show("Insert Clientt's Phone", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(emailTextBox.Text))
            {
                MessageBox.Show("Insert Client's Email", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }

            return output;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
