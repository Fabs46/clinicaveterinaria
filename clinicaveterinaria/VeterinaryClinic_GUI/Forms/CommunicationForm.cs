﻿
namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class CommunicationForm : Form
    {
        Communication Communication;

        Communications Communications;

        Clients Clients;

        Client Client;

        public CommunicationForm(Communication Communication, Communications Communications, Clients Clients,Client Client)
        {
            this.Communication = Communication;
            this.Communications = Communications;
            this.Clients = Clients;
            this.Client = Client;

            InitializeComponent();

            InitList();
        }


        private void sendMessageButton_Click(object sender, EventArgs e)
        {

            if (ValidationForm())
            {
                this.Client = (Client)clientsComboBox.SelectedItem;

                Communications.AddCommunication(Client,MessageRichTextBox.Text);

                MessageRichTextBox.Text = string.Empty;
                InitList();// from this form
            }

            else
            {
                MessageBox.Show("Fill correctly the fields and try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void deleteButton_Click(object sender, EventArgs e)
        {
            Communication communicationRemove = Communications.DeleteCom((Communication)MessagesListBox.SelectedItem);

            if (communicationRemove != null)
            {
                DialogResult answer;
                answer = MessageBox.Show($"Do you really want to delete message {communicationRemove.IdCom}",
                "Delete",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question);

                if (DialogResult.OK == answer)
                {
                    Communications.ListCommunications.Remove(communicationRemove);
                    Communications.SaveInfo();
                    InitList();
                }
            }
            else if (communicationRemove == null)
            {
                MessageBox.Show($"Select Message","Note:");
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Communication CommmunicationSelected;

            CommmunicationSelected = (Communication)MessagesListBox.SelectedItem;

            MessagesListBox.DisplayMember = "CommunicationInfo";

            clientsComboBox.DataSource = Clients.ListClients;
            clientsComboBox.SelectedItem = CommmunicationSelected.Client;

            
            if (MessagesListBox.SelectedItem == null)
            {
                MessageBox.Show($"Select a Line from the Message List");
                return;
            }

            else
            {
                MessagesListBox.DataSource = Communications.ListCommunications;
                MessageRichTextBox.Text = CommmunicationSelected.Message;// //To Do Selelected Liene

            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //Loads Lists info needed
        public void InitList()
        {
            clientsComboBox.DataSource = null;
            clientsComboBox.DataSource = Clients.ListClients;// getting ListClients from controller

            clientsComboBox.DisplayMember = "ClientInfo";
            clientsComboBox.SelectedItem = null;

            MessagesListBox.DataSource = null;
            MessagesListBox.DataSource = Communications.ListCommunications;
            MessagesListBox.DisplayMember = "CommunicationInfo";
            MessagesListBox.SelectedItem = null;

        }

        /// <summary>
        /// Validates inputs and entries
        /// </summary>
        private bool ValidationForm()
        {
            bool output = true;

            if (clientsComboBox.SelectedItem == null)
            {
                MessageBox.Show(" Select a Client", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            if (MessageRichTextBox.Text == string.Empty)
            {
                MessageBox.Show(" Blank Message", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            return output;
        }
    
    }
}
