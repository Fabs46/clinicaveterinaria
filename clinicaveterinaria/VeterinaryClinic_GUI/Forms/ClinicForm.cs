﻿namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class ClinicForm : Form
    {
        GUI Gui = new GUI();

        Clients Clients = new Clients();

        Client Client = new Client();

        Doctors Doctors = new Doctors();

        Doctor Doctor = new Doctor();

        Pets Pets = new Pets();

        Pet Pet = new Pet();

        Appointments Appointments = new Appointments();

        Appointment Appointment = new Appointment();

        Communications Communications = new Communications();

        Communication Communication = new Communication();


        public ClinicForm()
        {
            InitializeComponent();
            Doctors.ReadInfo();
            Clients.ReadInfo();
            Pets.ReadInfo(Clients);
            Appointments.ReadInfo();
            Communications.ReadInfo();
            labelDate.Text = DateTime.Today.ToString("dd-MM-yyyy");

        }

        private void doctorsButton_Click(object sender, EventArgs e)
        {

            DoctorForm doctorForm = new DoctorForm(Doctors, this, Appointments);

            doctorForm.ShowDialog();

            this.Show();

        }


        private void clientButton_Click(object sender, EventArgs e)
        {
            ClientPetForm ClientForm = new ClientPetForm(Clients, this, Pets,Client,Appointments);

            ClientForm.ShowDialog();

        }
        private void schedulingButton_Click(object sender, EventArgs e)
        {
            if (Doctors.ListDoctors.Count == 0)
            {
                MessageBox.Show("Add a Doctor first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Clients.ListClients.Count == 0)
            {
                MessageBox.Show("Add a Client first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Clients.newClient.Pets.Count == 0)
            {
                MessageBox.Show("A Client's Pet List is Empty, Please add a Pet first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                SchedulingForm schedulingForm = new SchedulingForm(Doctors, Clients, Pets, Appointments, Doctor, Client, Pet,Communications,Communication);
                schedulingForm.ShowDialog();
            }

        }

        private void MessagingButton_Click(object sender, EventArgs e)
        {

            if (Clients.ListClients.Count == 0)
            {
                MessageBox.Show("Add a Client first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;

            }

            else
            {
                CommunicationForm communicationForm = new CommunicationForm(Communication, Communications, Clients, Client);
                communicationForm.ShowDialog();
            }
        }

        private void ClinicForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Clients.SaveInfo();
            //Pets.SaveInfo(Clients.ListClients);
        }
    }
}
