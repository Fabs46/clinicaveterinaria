﻿
namespace LibraryVeterinaryClinic
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class Controller Communications
    /// </summary>
    public class Communications
    {
        //from the Model to get all properties
        public Communication newCommunication;

        //creating a new list Client to be used firstly on Method AddClient then onmore Methods and Forms
        public List<Communication> ListCommunications = new List<Communication>();

        int idCommunication = 1;//ID starts on 1

        /// <summary>
        /// add an Appointment to the ListCommunications ant increments the ID 
        /// (mainly used on schedulingForm and on ReadInfo())
        /// Saves into a txt File
        /// </summary>
        /// <param name="client">Receveing from the Form or the file</param>
        /// <param name="message">Receveing from the Form or the file</param>
        public void AddCommunication( Client client, string message)
        {
            newCommunication = new Communication
            {

                IdCom = idCommunication,
                Client = client,
                Message = message,
            };

            ListCommunications.Add(newCommunication);// adding values into the list, Pets inside of the model class Client 
            SaveInfo();

            idCommunication++;//id increment
        }

        /// <summary>
        /// Method to delete Pet returning a Blank Communication
        /// </summary>
        /// <param name="selectedMessage"></param>
        /// <returns></returns>
        public Communication DeleteCom (Communication selectedMessage)
        {

            Communication MessageBlank = null;

            if (selectedMessage != null)
            {
                foreach (Communication communication in ListCommunications)
                {
                    if (selectedMessage.IdCom == communication.IdCom)
                    {

                        MessageBlank = communication; //gets the doctor selected
                    }
                }

                return MessageBlank;
            }

            return MessageBlank;
        }


        /// <summary>
        /// Creates a File and adds lines sw
        /// </summary>
        public void SaveInfo()
        {
            string file = @"ComInfo.txt";

            StreamWriter sw = new StreamWriter(file, false);

            if (!File.Exists(file))
            {
                sw = File.CreateText(file);
            }

            foreach (Communication NewCom in ListCommunications)
            {
                string line = $"{NewCom.IdCom};{NewCom.Client.NameClient};{NewCom.Client.Phone};{NewCom.Client.Email};{NewCom.Message}";
                sw.WriteLine(line);
            }

            sw.Close();
        }

        /// <summary>
        /// Loads fields from a txt File (string[] fields) and uses the Method Add to add to the list 
        /// </summary>
        public void ReadInfo()
        {
            string file = @"ComInfo.txt";

            StreamReader sr;

            if (File.Exists(file))
            {
                sr = File.OpenText(file);
                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    newCommunication = new Communication();//needs to instance 

                    string[] fields = new string[4];

                    fields = line.Split(';');//split fields

                    newCommunication.IdCom = Convert.ToInt16(fields[0]);
                    newCommunication.Client = new Client
                    {
     
                        NameClient = (fields[1]),
                        Phone = Convert.ToInt32(fields[2]),
                        Email = fields[3],
                    };
                    newCommunication.Message = (fields[4]);

                    ListCommunications.Add(newCommunication);//Add an new Com object into the list

                    idCommunication = newCommunication.IdCom + 1;
                }
                sr.Close();
            }
        }

    }
}
