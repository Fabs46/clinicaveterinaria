﻿
namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class EditAppointmentForm : Form
    {
        Appointment Appointment;

        Appointments Appointments;

        SchedulingForm SchedulingForm;

        Doctors Doctors;

        Doctor Doctor;

        Clients Clients;

        Client Client;

        Pet Pet;

        Communications Communications;

        public EditAppointmentForm(SchedulingForm form, Appointments Appointments, Appointment Appointment, Doctors Doctors, Doctor Doctor,
            Clients Clients, Client Client, Pet Pet, Communications Communications)
        {
            InitializeComponent();
            this.Appointment = Appointment;
            this.Appointments = Appointments;
            this.SchedulingForm = form;
            this.Doctors = Doctors;
            this.Clients = Clients;
            this.Client = Client;
            this.Doctor = Doctor;
            this.Communications = Communications;

            InitInfo();

        }



        private void cancelButton_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        /// <summary>
        /// Loads all the needed info
        /// </summary>

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {

            monthCalendar.SelectionStart = monthCalendar.SelectionEnd;// impossible to range calendar
        }

        private void clientsComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (clientsComboBox.SelectedItem != null)
            {
                Client selectedClient = (Client)clientsComboBox.SelectedItem;

                petsComboBox.DataSource = null;
                petsComboBox.DataSource = selectedClient.Pets;
            }

            petsComboBox.DisplayMember = "PetInfo";
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            this.Doctor = (Doctor)doctorsComboBox.SelectedItem;
            this.Client = (Client)clientsComboBox.SelectedItem;
            this.Pet = (Pet)petsComboBox.SelectedItem;

            if (ValidationForm())
            {

                Appointments.Edition(Appointment, Doctor, Client, Pet,
                    appointmentTypeComboBox.Text, monthCalendar.SelectionStart.Date.ToString("yyyy-MM-dd"), Convert.ToInt16(hoursNumericUpDown.Text));//receiving the edition from the controller


                Appointments.SaveInfo();
                SchedulingForm.InitList();


                this.Close();
            }
        }

        private void InitInfo()
        {

            appointmentIdTextBox.Text = Appointment.IdAppointment.ToString();

            doctorsComboBox.DataSource = null;
            doctorsComboBox.DataSource = Doctors.ListDoctors;// getting ListDoctors from controller
            doctorsComboBox.DisplayMember = "DoctorInfo";

            clientsComboBox.DataSource = null;
            clientsComboBox.DataSource = Clients.ListClients;// getting ListClients from controller
            clientsComboBox.DisplayMember = "ClientInfo";



            appointmentTypeComboBox.Text = Appointment.AppointmentType;
            hoursNumericUpDown.Text = Appointment.AppointmentHour.ToString();

            doctorsComboBox.SelectedItem = Appointment.Doctor;
            clientsComboBox.SelectedItem = Appointment.Client;
            petsComboBox.SelectedItem = Appointment.Pet;
        }

        /// <summary>
        /// Validation method
        /// </summary>
        /// <returns>output bool if true keeps doing the task and just returns true , if false stops the task and display the Error Messages</returns>
        private bool ValidationForm()
        {
            bool output = true;


            if (hoursNumericUpDown.Value >= 20)
            {
                MessageBox.Show(" Appointments available until 19:00 Hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            if (monthCalendar.SelectionStart.Date == DateTime.Today)
            {
                MessageBox.Show(" Appointments available starting tomorrow", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (Appointments.newAppointment.Date == monthCalendar.SelectionEnd.Date.AddDays(1).ToString("dd-MM-yyyy"))
                {
                    string message = $"Mr(s) {Client.SurnameClient} don't forget your {appointment.AppointmentType} Appointment tomorrow at {appointment.AppointmentHour} O'clock for your pet {appointment.Pet.NamePet}";

                    foreach (Communication Com in Communications.ListCommunications)
                    {
                        if (Com.Message == message)
                        {
                            output = false;
                        }
                        else
                        {
                            Communications.AddCommunication(Client, message); MessageBox.Show("tomorrow test ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                    }

                    output = false;
                    return output;
                }
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Doctor == doctorsComboBox.SelectedItem && appointment.Pet == petsComboBox.SelectedItem &&
                    appointment.AppointmentHour == Convert.ToInt16(hoursNumericUpDown.Text) && Appointments.newAppointment.Date == monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"))
                {
                    MessageBox.Show("Create another appointment", "Repeated Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Pet == petsComboBox.SelectedItem &&
                    appointment.AppointmentHour == Convert.ToInt16(hoursNumericUpDown.Text) && Appointments.newAppointment.Date == monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"))
                {
                    MessageBox.Show("Create another appointment", "That Pet is already with one Appointment", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Doctor == doctorsComboBox.SelectedItem &&
                    appointment.AppointmentHour == Convert.ToInt16(hoursNumericUpDown.Text) && Appointments.newAppointment.Date == monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"))
                {
                    MessageBox.Show("Create another appointment", "That Doctor is already with one Appointment", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    output = false;
                    return output;
                }
            }
            foreach (Doctor doctor in Doctors.ListDoctors)
            {
                if (Convert.ToInt32(hoursNumericUpDown.Value) > doctor.EndShift)
                {
                    Doctor = (Doctor)doctorsComboBox.SelectedItem;
                    if (Doctor == doctor)
                    {
                        MessageBox.Show($"This Doctor ends at {doctor.EndShift + 1}", "Info measleading", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        output = false;
                        return output;
                    }
                }
            }
            foreach (Doctor doctor in Doctors.ListDoctors)
            {
                if (Convert.ToInt32(hoursNumericUpDown.Value) < doctor.StartShift)
                {
                    Doctor = (Doctor)doctorsComboBox.SelectedItem;
                    if (Doctor == doctor)
                    {
                        MessageBox.Show($"This Doctor starts at {doctor.StartShift}", "Info measleading", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        output = false;
                        return output;
                    }
                }
            }

            if (monthCalendar.SelectionStart < DateTime.Today)
            {
                MessageBox.Show("Impossible Date", "Choose a proper day", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }



            if (String.IsNullOrEmpty(doctorsComboBox.Text))
            {
                MessageBox.Show("Select the Doctor you want", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(clientsComboBox.Text))
            {
                MessageBox.Show("Select the Client you want", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(petsComboBox.Text))
            {
                MessageBox.Show("Select the Pet you want", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            DateTime hour = DateTime.UtcNow;
            if (hoursNumericUpDown.Text == hour.ToString("HH") && DateTime.Today == monthCalendar.SelectionStart)
            {
                MessageBox.Show("Hour TEST", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            return output;
        }

    }
}
