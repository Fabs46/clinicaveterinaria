﻿namespace VeterinaryClinic_GUI.Forms
{

    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class EditPetForm : Form
    {
        Pet Pet;

        Pets Pets;

        ClinicForm ClinicForm;

        ClientPetForm ClientForm;

        Clients Clients;

        Client Client;

        public EditPetForm(ClientPetForm ClientForm, Pets Pets, ClinicForm ClinicForm, Pet Pet,Client Client,Clients Clients)
        {
            

            InitializeComponent();
            this.Pet = Pet;
            this.Pets = Pets;
            this.ClientForm = ClientForm;
            this.ClinicForm = ClinicForm;
            this.Client = Client;
            this.Clients = Clients;


            petIdTextBox.Text = this.Pet.IdPet.ToString();
            petNameTextBox.Text = this.Pet.NamePet;
            ageNumericUpDown.Value = this.Pet.Age;
            weightNumericUpDown.Text = this.Pet.Weight.ToString();

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void saveButton_Click_1(object sender, EventArgs e)
        {
            ///receiving the edition from the controller with validation method on this form
            if (ValidationForm())
            {



                Pets.EditionPet(Pet, petNameTextBox.Text, ageNumericUpDown.Text, weightNumericUpDown.Text);//receiving the edition from the controller

                Pets.SaveInfo(Clients.ListClients);
                ClientForm.InitList();

                this.Close();

            }
        }

        /// <summary>
        /// Validation method
        /// </summary>
        /// <returns>output bool if true keeps doing the task and just returns true , if false stops the task and display the Error Messages</returns>
        private bool ValidationForm()
        {
            bool output = true;

            if (String.IsNullOrEmpty(petNameTextBox.Text))
            {
                MessageBox.Show("Insert Pet's name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(ageNumericUpDown.Text))
            {
                MessageBox.Show("Insert Pet's age", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(weightNumericUpDown.Text))
            {
                MessageBox.Show("Insert Pet's weight", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            return output;
        }

    }
}
