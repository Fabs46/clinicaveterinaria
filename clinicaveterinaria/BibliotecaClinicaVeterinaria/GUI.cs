﻿namespace LibraryVeterinaryClinic
{
    using System.Windows.Forms;

    /// <summary>
    /// Class GUI to correspond Methods just for the Forms
    /// </summary>
    public class GUI
    {

        public void EnableControls(Control con)
        {
            foreach (Control c in con.Controls)
            {
                EnableControls(c);
            }
            con.Enabled = true;
        }

        public void DisableControls(Control con)
        {
            foreach (Control c in con.Controls)
            {
                DisableControls(c);
            }
            con.Enabled = false;
        }
    }
}
