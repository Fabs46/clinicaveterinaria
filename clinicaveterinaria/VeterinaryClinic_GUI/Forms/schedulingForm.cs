﻿
namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class SchedulingForm : Form
    {
        Doctor Doctor;
        Doctors Doctors;

        Client Client;
        Clients Clients;

        Pet Pet;
        Pets Pets;

        Appointments Appointments;

        Communication Communication;
        Communications Communications;

        public SchedulingForm(Doctors Doctors, Clients Clients, Pets Pets, Appointments Appointments, Doctor Doctor,
            Client Client, Pet Pet, Communications Communications, Communication Communication)
        {
            this.Doctors = Doctors;
            this.Clients = Clients;
            this.Pets = Pets;
            this.Appointments = Appointments;
            this.Communication = Communication;
            this.Communications = Communications;


            InitializeComponent();
            InitList();


        }


        private void scheduleButton_Click(object sender, EventArgs e)
        {


            this.Doctor = (Doctor)doctorsComboBox.SelectedItem;
            this.Client = (Client)clientsComboBox.SelectedItem;
            this.Pet = (Pet)petsComboBox.SelectedItem;

            if (ValidationForm())
            {

                Appointments.AddAppointment(Doctor, Client, Pet, appointmentTypeComboBox.Text, monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"),
                    Convert.ToInt32(hoursNumericUpDown.Value));

                var today = DateTime.Today;
                monthCalendar.SelectionStart = monthCalendar.SelectionEnd;

                doctorsComboBox.SelectedItem = null;
                clientsComboBox.SelectedItem = null;
                petsComboBox.SelectedItem = null;
                appointmentTypeComboBox.SelectedItem = null;

                InitList();// from this form
            }
            else
            {
                MessageBox.Show("Fill correctly the fields and try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (appointmentListBox.SelectedItem == null)
            {
                MessageBox.Show("Select an appointment from the list please", "Info measleading", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                Appointment AppointmentToEdit;
                AppointmentToEdit = new Appointment();
                AppointmentToEdit = (Appointment)appointmentListBox.SelectedItem;


                EditAppointmentForm EditAppointmentForm = new EditAppointmentForm(this, Appointments, AppointmentToEdit, Doctors, Doctor, Clients, Client, Pet, Communications);

                EditAppointmentForm.ShowDialog();
                this.Show();
            }
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            Appointment appointmentRemove = Appointments.DeleteAppointment((Appointment)appointmentListBox.SelectedItem);

            if (appointmentRemove != null)
            {
                DialogResult answer;
                answer = MessageBox.Show($"Do you really want to delete the selected appointment?",
                    "Delete",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question);

                if (DialogResult.OK == answer)
                {
                    Appointments.ListAppointments.Remove(appointmentRemove);
                    Appointments.SaveInfo();
                    InitList();
                }

            }
        }

        /// <summary>
        /// Selection Event, to shows the Pet for each selected Client
        /// </summary>
        private void clientsComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (clientsComboBox.SelectedItem != null)
            {
                Client selectedClient = (Client)clientsComboBox.SelectedItem;

                petsComboBox.DataSource = null;
                petsComboBox.DataSource = selectedClient.Pets;
            }

            petsComboBox.DisplayMember = "PetInfo";

        }

        /// <summary>
        /// Evente Month Calendar, 
        /// </summary>
        private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            monthCalendar.SelectionStart = monthCalendar.SelectionEnd;// impossible to range calendar
        }

        /// <summary>
        /// Loads the needed Lists
        /// </summary>
        public void InitList()
        {
            monthCalendar.MinDate = DateTime.Today;

            doctorsComboBox.DataSource = null;
            doctorsComboBox.DataSource = Doctors.ListDoctors;// getting ListDoctors from controller

            doctorsComboBox.DisplayMember = "DoctorInfo";

            clientsComboBox.DataSource = null;
            clientsComboBox.DataSource = Clients.ListClients;// getting ListClients from controller

            clientsComboBox.DisplayMember = "ClientInfo";

            doctorsComboBox.SelectedItem = null;
            clientsComboBox.SelectedItem = null;

            petsComboBox.DataSource = null;

            appointmentListBox.DataSource = null;
            appointmentListBox.DataSource = Appointments.ListAppointments;

            appointmentListBox.DisplayMember = "AppointmentInfo";
        }

        /// <summary>
        /// Validation method
        /// </summary>
        /// <returns>output bool if true keeps doing the task and just returns true , if false stops the task and display the Error Messages</returns>
        private bool ValidationForm()
        {
            bool output = true;

            if (hoursNumericUpDown.Value >= 20)
            {
                MessageBox.Show(" Appointments available until 19:00 Hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }

            if (monthCalendar.SelectionStart.Date == DateTime.Today)
            {
                MessageBox.Show(" Appointments available starting tomorrow", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
 
            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Doctor == doctorsComboBox.SelectedItem && appointment.Pet == petsComboBox.SelectedItem &&
                    appointment.AppointmentHour == Convert.ToInt16(hoursNumericUpDown.Text) && Appointments.newAppointment.Date == monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"))
                {
                    MessageBox.Show("Create another appointment", "Repeated Info", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    output = false;
                    return output;
                }
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Pet == petsComboBox.SelectedItem &&
                    appointment.AppointmentHour == Convert.ToInt16(hoursNumericUpDown.Text) && Appointments.newAppointment.Date == monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"))
                {
                    MessageBox.Show("Create another appointment", "That Pet is already with one Appointment", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    output = false;
                    return output;

                }
            }

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Doctor == doctorsComboBox.SelectedItem &&
                    appointment.AppointmentHour == Convert.ToInt16(hoursNumericUpDown.Text) && Appointments.newAppointment.Date == monthCalendar.SelectionStart.Date.ToString("dd-MM-yyyy"))
                {
                    MessageBox.Show("Create another appointment", "That Doctor is already with one Appointment", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    output = false;
                    return output;
                }
            }
            foreach (Doctor doctor in Doctors.ListDoctors)
            {
                if (Convert.ToInt32(hoursNumericUpDown.Value) > doctor.EndShift)
                {
                    Doctor = (Doctor)doctorsComboBox.SelectedItem;
                    if (Doctor == doctor)
                    {
                        MessageBox.Show($"This Doctor ends at {doctor.EndShift + 1}", "Info measleading", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        output = false;
                        return output;
                    }
                }
            }
            foreach (Doctor doctor in Doctors.ListDoctors)
            {
                if (Convert.ToInt32(hoursNumericUpDown.Value) < doctor.StartShift)
                {
                    Doctor = (Doctor)doctorsComboBox.SelectedItem;
                    if (Doctor == doctor)
                    {
                        MessageBox.Show($"This Doctor starts at {doctor.StartShift}", "Info measleading", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        output = false;
                        return output;
                    }
                }
            }

            if (monthCalendar.SelectionStart < DateTime.Today)
            {
                MessageBox.Show("Impossible Date", "Choose a proper day", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            if (!string.IsNullOrEmpty(doctorsComboBox.Text) && appointmentListBox.Items.Contains(doctorsComboBox.Text))
            {
                MessageBox.Show("Create another appointment", "That Doctor is already with appointment", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            if (String.IsNullOrEmpty(doctorsComboBox.Text))
            {
                MessageBox.Show("Select the Doctor you want", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(clientsComboBox.Text))
            {
                MessageBox.Show("Select the Client you want", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(petsComboBox.Text))
            {
                MessageBox.Show("Select the Pet you want", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;

            }

            DateTime hour = DateTime.UtcNow;
            if (hoursNumericUpDown.Text == hour.ToString("HH") && DateTime.Today == monthCalendar.SelectionStart)
            {
                MessageBox.Show("Hour TEST", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
  
            return output;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
