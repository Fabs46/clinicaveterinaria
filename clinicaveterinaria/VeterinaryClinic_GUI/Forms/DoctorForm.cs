﻿
namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class DoctorForm : Form
    {

        Doctors Doctors;

        ClinicForm ClinicForm;

        Appointments Appointments;

        GUI Gui = new GUI();
         
        public DoctorForm( Doctors Doctors, ClinicForm ClinicForm, Appointments Appointments)
        {

            this.Doctors = Doctors;
            this.ClinicForm = ClinicForm;
            this.Appointments = Appointments;
            InitializeComponent();

            InitList();

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (ValidationForm())
            {
                Doctors.AddDoctor(doctorNameTextBox.Text, doctorSurnameTextBox.Text, doctorRoomTextBox.Text, Convert.ToInt16(startShiftNumericUpDown.Text ), Convert.ToInt16(endShiftNumericUpDown.Text));

                InitList();// from this form
                doctorNameTextBox.Text = string.Empty;
                doctorSurnameTextBox.Text = string.Empty;
                doctorRoomTextBox.Text = string.Empty;
            }
            else
            {
                 MessageBox.Show("Fill correctly the fields and try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Doctor doctorRemove = Doctors.DeleteDoctor((Doctor)DoctorsListBox.SelectedItem);//Object created to connect to the Method in Controller

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Doctor.IdDoctor == doctorRemove.IdDoctor)
                {
                    MessageBox.Show("Appointments pending on this Vet", "Impossible to Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            if (doctorRemove != null)
            {
                DialogResult answer;
                answer = MessageBox.Show($"Do you really want to delete {doctorRemove.DoctorInfo}",
                "Delete",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question);

                if (DialogResult.OK == answer)
                {
                    Doctors.ListDoctors.Remove(doctorRemove);
                    Doctors.SaveInfo();
                    InitList();
                }
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Doctor doctorSelected = Doctors.DeleteDoctor((Doctor)DoctorsListBox.SelectedItem);

            //Opens the EditDoctorForm - sending the controller, the Clinic Form and the Method ont he Contoller with the selected line
            EditDoctorForm EditDoctorForm = new EditDoctorForm(this, Doctors, ClinicForm, Doctors.ToEditLine((Doctor)DoctorsListBox.SelectedItem));

            foreach (Appointment appointment in Appointments.ListAppointments)
            {
                if (appointment.Doctor.IdDoctor == doctorSelected.IdDoctor)
                {
                    MessageBox.Show("Appointments pending on this Vet, impossible to change the shit and Room", "Impossible to Edit", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            EditDoctorForm.ShowDialog();
            this.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (doctorNameTextBox.Text != string.Empty)
            {
                MessageBox.Show("Do you want to cancel the operation?", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            doctorNameTextBox.Text = string.Empty;
            doctorSurnameTextBox.Text = string.Empty;
            doctorRoomTextBox.Text = string.Empty;
        }


        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Loads needed Lists
        /// </summary>
        public void InitList()
        {
            DoctorsListBox.DataSource = null;
            DoctorsListBox.DataSource = Doctors.ListDoctors;// getting ListDoctors from controller
            DoctorsListBox.DisplayMember = "DoctorInfo";
        }

        /// <summary>
        /// Validation method
        /// </summary>
        /// <returns>output bool if true keeps doing the task and just returns true , if false stops the task and display the Error Messages</returns>
        private bool ValidationForm()
        {
            bool output = true;

            if (startShiftNumericUpDown.Value >= endShiftNumericUpDown.Value)
            {
                MessageBox.Show("Start Shift is transpassing the limit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (endShiftNumericUpDown.Value - startShiftNumericUpDown.Value <4)
            {
                MessageBox.Show("Less than 4 hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (endShiftNumericUpDown.Value - startShiftNumericUpDown.Value > 9)
            {
                MessageBox.Show("More than 9 hours", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(doctorNameTextBox.Text))
            {
                MessageBox.Show("Insert Doctor's name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(doctorSurnameTextBox.Text))
            {
                MessageBox.Show("Insert Doctor's surname", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }
            if (String.IsNullOrEmpty(doctorRoomTextBox.Text))
            {
                MessageBox.Show("Insert Doctor's room", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
                return output;
            }

            foreach (Doctor doctor in Doctors.ListDoctors)
            {
                if (doctorRoomTextBox.Text == doctor.Room)
                {
                    MessageBox.Show("Room Ocupied", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    output = false;
                    return output;
                }
            }

            return output;
        }
    }
}
