﻿
namespace LibraryVeterinaryClinic
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class Controller Appointments
    /// </summary>
    public class Appointments
    {
        //from the Model to get all properties
        public Appointment newAppointment;

        //creating a new list Client to be used firstly on Method AddClient then onmore Methods and Forms
        public List<Appointment> ListAppointments = new List<Appointment>();

        int idAppointment = 1;//ID starts on 1


        /// <summary>
        /// add an Appointment to the ListAppointments ant increments the ID 
        /// (mainly used on schedulingForm and on ReadInfo())
        /// Saves into a txt File
        /// </summary>
        /// <param name="doctor">Receveing from the Form or the file</param>
        /// <param name="client">Receveing from the Form or the file</param>
        /// <param name="pet">Receveing from the Form or the file</param>
        /// <param name="appointmentType">Receveing from the Form or the file</param>
        /// <param name="date">Receveing from the Form or the file</param>
        /// <param name="hour">Receveing from the Form or the file</param>
        public void AddAppointment(Doctor doctor, Client client, Pet pet, string appointmentType, string date, int hour)
        {
            newAppointment = new Appointment
            {

                IdAppointment = idAppointment,
                Doctor = doctor,
                Client = client,
                Pet = pet,

                AppointmentType = appointmentType,
                Date = date,
                AppointmentHour = hour,
            };

            ListAppointments.Add(newAppointment);// adding values into the list, Pets inside of the model class Client 
            SaveInfo();

            idAppointment++;//id increment
        }

        /// <summary>
        /// Method to delete Pet returning a Blank Pet
        /// </summary>
        /// <param name="selectedAppointment">Receiving from Form the line Selected</param>
        /// <returns></returns>
        public Appointment DeleteAppointment(Appointment selectedAppointment)
        {

            Appointment AppointmentBlank = null;


            if (selectedAppointment != null)
            {
                foreach (Appointment appointment in ListAppointments)
                {
                    if (selectedAppointment.IdAppointment == appointment.IdAppointment)
                    {

                        AppointmentBlank = appointment; //gets the doctor selected
                    }
                }

                return AppointmentBlank;
            }

            return AppointmentBlank;
        }

        /// <summary>
        /// Method to return the selected Appointment for Edition
        /// </summary>
        /// <param name="appointmentSelected"> Appointment received from the form</param>
        /// <returns>selected</returns>
        public Appointment ToEditLine(Appointment appointmentSelected)
        {
            Appointment selected = null;

            if (appointmentSelected != null)
            {
                foreach (Appointment Appointment in ListAppointments)
                {
                    if (appointmentSelected.IdAppointment == Appointment.IdAppointment)
                    {
                        selected = Appointment; //gets the doctor selected
                    }
                }
            }
            return selected;
        }

        /// <summary>
        ///  Edits the received List(selected the Line) and changes from EditForm
        /// </summary>
        /// <param name="appointment">Receiving from Form</param>
        /// <param name="doctor">Receiving from Form</param>
        /// <param name="client">Receiving from Form</param>
        /// <param name="pet">Receiving from Form</param>
        /// <param name="appointmentType">Receiving from Form</param>
        /// <param name="date">Receiving from Form</param>
        /// <param name="appointmentHour">Receiving from Form</param>
        public void Edition(Appointment appointment, Doctor doctor, Client client, Pet pet, string appointmentType, string date, int appointmentHour)
        {
            appointment.Doctor = doctor;
            appointment.Client = client;
            appointment.Pet = pet;
            appointment.AppointmentType = appointmentType;
            appointment.Date = date;
            appointment.AppointmentHour = appointmentHour;
        }


        /// <summary>
        /// Creates a File and adds lines sw
        /// </summary>
        public void SaveInfo()
        {
            string file = @"AppointmentInfo.txt";

            StreamWriter sw = new StreamWriter(file, false);

            if (!File.Exists(file))
            {
                sw = File.CreateText(file);
            }

            foreach (Appointment newAppointment in ListAppointments)
            {
                string line = $"{newAppointment.IdAppointment};" +
                    $"{newAppointment.Doctor.IdDoctor};{newAppointment.Doctor.NameDoctor};{newAppointment.Doctor.SurnameDoctor};" +
                    $"{newAppointment.Doctor.Room};{newAppointment.Doctor.StartShift};{newAppointment.Doctor.EndShift};" +
                    $"{newAppointment.Client.IdClient};{newAppointment.Client.NameClient};{newAppointment.Client.SurnameClient};" +
                    $"{newAppointment.Client.NIF};{newAppointment.Client.AdressClient};{newAppointment.Client.Phone};{newAppointment.Client.Email};" +
                    $"{newAppointment.Pet.IdPet};{newAppointment.Pet.AnimalType};{newAppointment.Pet.NamePet};{newAppointment.Pet.Age};{newAppointment.Pet.Weight};" +
                    $"{newAppointment.AppointmentType};{newAppointment.Date};{newAppointment.AppointmentHour}";
                sw.WriteLine(line);
            }

            sw.Close();
        }

        /// <summary>
        /// Loads fields from a txt File (string[] fields) and uses the Method Add to add to the list 
        /// </summary>
        public void ReadInfo()
        {
            string file = @"AppointmentInfo.txt";

            StreamReader sr;

            if (File.Exists(file))
            {
                sr = File.OpenText(file);
                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    newAppointment = new Appointment();//needs to instance 

                    string[] fields = new string[22];

                    fields = line.Split(';');//split fields

                    newAppointment.IdAppointment = Convert.ToInt32(fields[0]);

                    newAppointment.Doctor = new Doctor
                    {
                        IdDoctor = Convert.ToInt32(fields[1]),
                        NameDoctor = (fields[2]),
                        SurnameDoctor = (fields[3]),
                        Room = (fields[4]),
                        StartShift = Convert.ToInt32(fields[5]),
                        EndShift = Convert.ToInt32(fields[6]),
                    };

                    newAppointment.Client = new Client
                    {
                        IdClient = Convert.ToInt32(fields[7]),
                        NameClient = (fields[8]),
                        SurnameClient = (fields[9]),
                        NIF = (fields[10]),
                        AdressClient = (fields[11]),
                        Phone = Convert.ToInt32(fields[12]),
                        Email = fields[13],
                    };

                    newAppointment.Pet = new Pet
                    {

                        IdPet = Convert.ToInt32(fields[14]),
                        AnimalType = fields[15],
                        NamePet = (fields[16]),
                        Age = Convert.ToInt32(fields[17]),
                        Weight = Convert.ToDouble(fields[18]),
                    };


                    newAppointment.AppointmentType = (fields[19]);
                    newAppointment.Date = (fields[20]);
                    newAppointment.AppointmentHour = Convert.ToInt16((fields[21]));

                    ListAppointments.Add(newAppointment);//Add an new doctor object into the list

                    idAppointment = newAppointment.IdAppointment + 1;
                }
                sr.Close();
            }
        }

    }
}
