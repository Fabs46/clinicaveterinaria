﻿namespace VeterinaryClinic_GUI.Forms
{

    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class EditClientForm : Form
    {

        Client Client;

        Clients Clients;

        ClientPetForm ClientForm;

        ClinicForm ClinicForm;

        public EditClientForm(ClientPetForm ClientForm, Clients Clients, ClinicForm ClinicForm, Client Client)
        {
            InitializeComponent();
            this.Client = Client;
            this.Clients = Clients;
            this.ClientForm = ClientForm;
            this.ClinicForm = ClinicForm;


            InitInfo();
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void saveButton_Click_1(object sender, EventArgs e)
        {
            ///receiving the edition from the controller with validation method on this form
            if (ValidationForm())
            {

                Clients.Edition(Client, clientNameTextBox.Text,
                                  clientSurnameTextBox.Text,
                                  adressTextBox.Text,
                                  nifMaskedTextBox.Text,
                                  Convert.ToInt16(phoneMaskedTextBox.Text),
                                  emailTextBox.Text
                                  );

                Clients.SaveInfo();
                ClientForm.InitList();

                this.Close();

            }
        }

        /// <summary>
        /// All the Info needed from the form ClientPetForm loads here
        /// </summary>
        private void InitInfo()
        {
            clientIDTextBox.Text = Client.IdClient.ToString();//ToDO Line Selected
            clientNameTextBox.Text = Client.NameClient;
            clientSurnameTextBox.Text = Client.SurnameClient;
            adressTextBox.Text = Client.AdressClient;
            nifMaskedTextBox.Text = Client.NIF;
            emailTextBox.Text = Client.Email;
            phoneMaskedTextBox.Text = Client.Phone.ToString();
        }

        /// <summary>
        /// Validation method
        /// </summary>
        /// <returns>output bool if true keeps doing the task and just returns true , if false stops the task and display the Error Messages</returns>
        private bool ValidationForm()
        {
            bool output = true;

            if (String.IsNullOrEmpty(clientNameTextBox.Text))
            {
                MessageBox.Show("Insert Client's name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(clientSurnameTextBox.Text))
            {
                MessageBox.Show("Insert Client's surname", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(adressTextBox.Text))
            {
                MessageBox.Show("Insert Client's adress", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(nifMaskedTextBox.Text))
            {
                MessageBox.Show("Insert Client's NIF", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(phoneMaskedTextBox.Text))
            {
                MessageBox.Show("Insert Clientt's Phone", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }
            if (String.IsNullOrEmpty(emailTextBox.Text))
            {
                MessageBox.Show("Insert Client's Email", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }

            return output;
        }

    }
}
