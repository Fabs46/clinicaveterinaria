﻿namespace VeterinaryClinic_GUI.Forms
{
    partial class DoctorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.doctorNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorIDTextBox = new System.Windows.Forms.TextBox();
            this.doctorRoomTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.DoctorsListBox = new System.Windows.Forms.ListBox();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.doctorSurnameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.backButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.startShiftNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.endShiftNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.startShiftNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endShiftNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Doctor\'s Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Doctor\'s ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Doctor\'s Room";
            // 
            // doctorNameTextBox
            // 
            this.doctorNameTextBox.Location = new System.Drawing.Point(146, 43);
            this.doctorNameTextBox.Name = "doctorNameTextBox";
            this.doctorNameTextBox.Size = new System.Drawing.Size(122, 20);
            this.doctorNameTextBox.TabIndex = 1;
            // 
            // doctorIDTextBox
            // 
            this.doctorIDTextBox.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.doctorIDTextBox.Enabled = false;
            this.doctorIDTextBox.Location = new System.Drawing.Point(146, 9);
            this.doctorIDTextBox.Name = "doctorIDTextBox";
            this.doctorIDTextBox.Size = new System.Drawing.Size(122, 20);
            this.doctorIDTextBox.TabIndex = 4;
            // 
            // doctorRoomTextBox
            // 
            this.doctorRoomTextBox.Location = new System.Drawing.Point(146, 106);
            this.doctorRoomTextBox.Name = "doctorRoomTextBox";
            this.doctorRoomTextBox.Size = new System.Drawing.Size(122, 20);
            this.doctorRoomTextBox.TabIndex = 3;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(31, 225);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(193, 225);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // DoctorsListBox
            // 
            this.DoctorsListBox.FormattingEnabled = true;
            this.DoctorsListBox.Location = new System.Drawing.Point(327, 12);
            this.DoctorsListBox.Name = "DoctorsListBox";
            this.DoctorsListBox.Size = new System.Drawing.Size(402, 134);
            this.DoctorsListBox.TabIndex = 8;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(636, 169);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(93, 47);
            this.editButton.TabIndex = 9;
            this.editButton.Text = "Edit Selected Doc";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(327, 169);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(93, 47);
            this.deleteButton.TabIndex = 10;
            this.deleteButton.Text = "Delete Selected Doc";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // doctorSurnameTextBox
            // 
            this.doctorSurnameTextBox.Location = new System.Drawing.Point(146, 73);
            this.doctorSurnameTextBox.Name = "doctorSurnameTextBox";
            this.doctorSurnameTextBox.Size = new System.Drawing.Size(122, 20);
            this.doctorSurnameTextBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Doctor\'s Surname";
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(265, 254);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(63, 21);
            this.backButton.TabIndex = 8;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Doctor\'s Start Shift";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Doctor\'s End Shift";
            // 
            // startShiftNumericUpDown
            // 
            this.startShiftNumericUpDown.Location = new System.Drawing.Point(146, 143);
            this.startShiftNumericUpDown.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.startShiftNumericUpDown.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.startShiftNumericUpDown.Name = "startShiftNumericUpDown";
            this.startShiftNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.startShiftNumericUpDown.TabIndex = 4;
            this.startShiftNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // endShiftNumericUpDown
            // 
            this.endShiftNumericUpDown.Location = new System.Drawing.Point(146, 177);
            this.endShiftNumericUpDown.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.endShiftNumericUpDown.Minimum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.endShiftNumericUpDown.Name = "endShiftNumericUpDown";
            this.endShiftNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.endShiftNumericUpDown.TabIndex = 5;
            this.endShiftNumericUpDown.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // DoctorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(764, 361);
            this.Controls.Add(this.endShiftNumericUpDown);
            this.Controls.Add(this.startShiftNumericUpDown);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.doctorSurnameTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.DoctorsListBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.doctorRoomTextBox);
            this.Controls.Add(this.doctorIDTextBox);
            this.Controls.Add(this.doctorNameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DoctorForm";
            this.Text = "Doctor";
            ((System.ComponentModel.ISupportInitialize)(this.startShiftNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endShiftNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox doctorNameTextBox;
        private System.Windows.Forms.TextBox doctorIDTextBox;
        private System.Windows.Forms.TextBox doctorRoomTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ListBox DoctorsListBox;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox doctorSurnameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown startShiftNumericUpDown;
        private System.Windows.Forms.NumericUpDown endShiftNumericUpDown;
    }
}

