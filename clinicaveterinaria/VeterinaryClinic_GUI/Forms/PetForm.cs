﻿namespace VeterinaryClinic_GUI.Forms
{
    using LibraryVeterinaryClinic;
    using System;
    using System.Windows.Forms;

    public partial class PetForm : Form
    {
        Pets Pets;

        ClinicForm ClinicForm;

        Clients Clients;

        ClientPetForm ClientForm;

        Client Client;

        public PetForm(Pets Pets, ClinicForm ClinicForm, Clients Clients, ClientPetForm ClientForm, Client Client)
        {
            this.ClientForm = ClientForm;
            this.Pets = Pets;
            this.ClinicForm = ClinicForm;
            this.Clients = Clients;
            this.Client = Client;

            InitializeComponent();
            InitList();

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Do you want to cancel the operation?", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            petNameTextBox.Text = string.Empty;
            animalComboBox.Text = string.Empty;
            ageNumericUpDown.Text = string.Empty;
            weightNumericUpDown.Text = string.Empty;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (validationForm() && Client != null)
            {
                Pets.AddPet(animalComboBox.Text, petNameTextBox.Text,
                Convert.ToInt16(ageNumericUpDown.Text), Convert.ToDouble(weightNumericUpDown.Text), Client, Clients);

                ClientForm.InitList();
                InitList();
            }
            else
            {
                MessageBox.Show("Fill correctly the fields and try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            animalComboBox.Text = string.Empty;
            petNameTextBox.Text = string.Empty;
            ageNumericUpDown.Value = 0;
            weightNumericUpDown.Value = 0;
            animalComboBox.SelectedItem = null;

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Shows the list
        /// </summary>
        public void InitList()
        {
            clientLabel.Text = $" {Client.IdClient} -> {Client.NameClient}  {Client.SurnameClient}";

            PetListBox.DataSource = null;
            PetListBox.DataSource = Client.Pets;
            PetListBox.DisplayMember = "PetInfo";
            PetListBox.SelectedItem = null;
            //getClientsListBox.DataSource = null;
            //getClientsListBox.DataSource = Clients.ListClients;
            //getClientsListBox.DisplayMember = "ClientInfo";




        }

        private bool validationForm()
        {
            bool output = true;

            if (String.IsNullOrEmpty(petNameTextBox.Text))
            {
                MessageBox.Show("Insert Pet's name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }

            if (String.IsNullOrEmpty(animalComboBox.Text))
            {
                MessageBox.Show("Insert Pet's kind", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }


            if (Convert.ToDecimal(weightNumericUpDown.Text) == 0)
            {
                MessageBox.Show("Insert Pet's weight", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }

            if (ageNumericUpDown.Value == 0)
            {
                MessageBox.Show("Insert Pet's age", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                output = false;
            }



            return output;
        }

    }
}
